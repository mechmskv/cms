from app import db

association_table = db.Table('association', db.Model.metadata,
    db.Column('group_id', db.Integer, db.ForeignKey('group.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('user.id'))
)

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    phone = db.Column(db.String(120), index=True, unique=True)
    groups = db.relationship(
        "Group",
        secondary=association_table,
        back_populates="users")

    def serialize(self):
        groups = [g.serialize() for g in self.groups]
        return {
            'id': self.id,
            'username': self.name,
            'phone': self.phone,
            'groups': groups
        }

class Group(db.Model):
    __tablename__ = 'group'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    path = db.Column(db.String(120), index=True, unique=False)
    level = db.Column(db.Integer, index=True)
    users = db.relationship(
        "User",
        secondary=association_table,
        back_populates="groups")

    def serialize(self):
        return {
            'id': self.id,
            'name': self.name,
            'path': self.path,
            'level': self.level
        }