from flask import request, render_template, jsonify, send_from_directory
from app import app

from app import db
from app.models import User, Group


@app.route('/')
@app.route('/index')
def index():
    return send_from_directory('templates', 'index.html')

@app.route('/users/get/all', methods=['GET'])
def get_users():
    users = User.query.all()
    users = [u.serialize() for u in users]
    
    return jsonify(users)

@app.route('/users/get/<int:user_id>', methods=['GET'])
def get_user(user_id):
    user = User.query.get(user_id)
    
    return jsonify( user.serialize() )

@app.route('/users/save/<int:user_id>', methods=['POST'])
def save_user(user_id):
    data = request.get_json(force=True)
    user = User.query.get(user_id)
    user.name = data.get('name')
    user.phone = data.get('phone')
    db.session.commit()
    
    return jsonify( user.serialize() )

@app.route('/users/create', methods=['POST'])
def create_user():
    data = request.get_json(force=True)
    new_name = data.get('name')
    new_phone = data.get('phone')
    user = User(name=new_name, phone=new_phone)
    db.session.add(user)
    db.session.commit()

    return jsonify( user.serialize() )

@app.route('/users/delete/<int:user_id>', methods=['POST'])
def delete_user(user_id):
    User.query.filter_by(id=user_id).delete()
    db.session.commit()

    users = User.query.all()
    users = [u.serialize() for u in users]
    
    return jsonify(users)


@app.route('/groups/get/all', methods=['GET'])
def get_groups():
    groups = Group.query.order_by('path').all()
    groups = [g.serialize() for g in groups]
    
    return jsonify(groups)

@app.route('/groups/get/<int:group_id>', methods=['GET'])
def get_group(group_id):
    group = Group.query.get(group_id)
    path = group.path
    arr = path.split('/')
    ids = [int(val) for val in arr if len(val)]
    ids.pop()
    groups = Group.query.filter( Group.id.in_((ids)) ).all()

    groups = [g.serialize() for g in groups]

    users = [u.serialize() for u in group.users]

    full_group = {
        'group': group.serialize(),
        'breadcrumbs': groups,
        'users': users
    }
    return jsonify( full_group )


@app.route('/users/bind/group/<int:user_id>/<int:group_id>', methods=['POST'])
def bind_group_to_user(user_id, group_id):
    user = User.query.get(user_id)
    group = Group.query.get(group_id)
    user.groups.append(group)
    db.session.commit()

    groups = user.groups
    groups = [g.serialize() for g in groups]
    
    return jsonify( groups )

@app.route('/users/unbind/group/<int:user_id>/<int:group_id>', methods=['POST'])
def unbind_group_from_user(user_id, group_id):
    user = User.query.get(user_id)
    group = Group.query.get(group_id)
    group_index = -1
    for i, user_group in enumerate(user.groups):
        if group == user_group:
            group_index = i

    if group_index >=0:
        user.groups.pop(group_index)

    db.session.commit()

    groups = user.groups
    groups = [g.serialize() for g in groups]
    
    return jsonify( groups )
